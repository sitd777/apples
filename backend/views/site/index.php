<?php

/* @var $this yii\web\View */

$this->title = 'Apple Application';

$this->params['breadcrumbs'][] = 'Яблоки';
?>
<div class="site-index">

    <div class="body-content">
        <p><a href="<?= \yii\helpers\Url::to(['site/crop']) ?>" class="btn btn-primary">Новый урожай <span class="glyphicon glyphicon-refresh"></span></a></p>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['class' => 'common\grid\AppleColorColumn', 'attribute' => 'color'],
                'size',
                ['class' => 'common\grid\AppleStatusColumn', 'attribute' => 'status'],
                ['class' => 'yii\grid\DataColumn', 'attribute' => 'created_at', 'format' => 'dateTime'],
                ['class' => 'yii\grid\DataColumn', 'attribute' => 'falleddown_at', 'format' => 'dateTime'],
                [
                    'class' => 'common\grid\ActionColumn',
                    'defaultClass' => 'btn btn-primary btn-sm',
                    'defaultShowTitle' => false,
                    'buttons' => [
                        'falldown' => ['icon' => 'glyphicon glyphicon-arrow-down', 'title' => 'Упасть', 'class' => 'btn btn-success btn-sm'],
                        'eat'      => ['icon' => 'glyphicon glyphicon-apple', 'title' => 'Съесть'],
                        'sep'      => [],
                        'delete'   => ['icon' => 'glyphicon glyphicon-trash', 'title' => 'Удалить', 'class' => 'btn btn-danger btn-sm', 'confirm' => 'Удаляем яблоко?', 'isPost' => true],
                    ],
                ],
            ],
        ]); ?>

    </div>
</div>
