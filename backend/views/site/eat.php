<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $title string */
/* @var $this yii\web\View */
/* @var $model common\models\AppleEatForm */

$this->title = $title;
$this->params['breadcrumbs'][] = 'Яблоки';
$this->params['breadcrumbs'][] = $title;
?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'eat')->textInput(['maxlength' => true, 'type' => 'number']) ?>
<p>Доступно <?= $left ?> % яблока.</p>

<div class="form-group">
    <?= Html::submitButton('Есть', ['class' => 'btn btn-success']) ?>
    <?php echo Html::a('Передумать', ['index'], ['class' => 'btn btn-warning']) ?>
</div>

<?php ActiveForm::end(); ?>


