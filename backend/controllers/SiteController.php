<?php
namespace backend\controllers;

use common\models\ActionModelInterface;
use common\models\Apple;
use common\models\AppleEatForm;
use common\models\AppleSearch;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    const APPLE_COUNT_MIN = 7;
    const APPLE_COUNT_MAX = 21;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'crop', 'falldown', 'eat', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AppleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * New crop action
     */
    public function actionCrop()
    {
        // Delete old apples
        $appleModels = Apple::find()->all();
        foreach($appleModels as $appleModel) $appleModel->delete();

        // Get number of new apples
        $appleCount = rand(self::APPLE_COUNT_MIN, self::APPLE_COUNT_MAX);

        // Create new apples
        for($i = 0; $i < $appleCount; $i++)
        {
            $newApple = new Apple();
            $newApple->save();
        }

        // Create information message
        $this->createFlash('Уродилось яблок: ' . $appleCount);

        // Return to apple list
        return $this->redirect(['index']);
    }

    /**
     * Apple fall down action
     *
     * @param integer $id
     */
    public function actionFalldown($id)
    {
        $appleModel = $this->getModel($id, 'falldown');

        if($appleModel) $appleModel->fallToGround();

        return $this->redirect(['index']);
    }

    /**
     * Apple eat action
     *
     * @param integer $id
     */
    public function actionEat($id)
    {
        $appleModel = $this->getModel($id, 'eat');

        if($appleModel)
        {
            $appleEatModel = new AppleEatForm($appleModel);
            if($appleEatModel->load(Yii::$app->request->post()) && $appleEatModel->validate() && $appleModel->eat($appleEatModel->eat))
            {
                if($appleModel->size > 0)
                    $this->createFlash('Поели яблока! Осталось: ' . ($appleModel->size * 100) . ' %', 'success');
                else
                    $this->createFlash('Съели всё яблоко!', 'warning');
            }
            else
            {
                return $this->render('eat', [
                    'title' => 'Едим яблоко',
                    'model' => $appleEatModel,
                    'left'  => intval($appleModel->size * 100),
                ]);
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Apple delete action
     *
     * @param integer $id
     */
    public function actionDelete($id)
    {
        $appleModel = $this->getModel($id, 'delete');

        if($appleModel)
        {
            $appleModel->delete();
            $this->createFlash('Яблоко удалено!', 'warning');
        }

        return $this->redirect(['index']);
    }

    /**
     * Returns apple model and checks rights for an action
     *
     * @param integer $id
     * @param string  $actionName
     * @return Apple or false
     */
    protected function getModel($id, $actionName)
    {
        $appleModel = Apple::findOne(intval($id));
        if(!$appleModel)
        {
            $this->createFlash('Яблоко не найдено на дереве! ID: ' . $id, 'error');
            return false;
        }

        if($appleModel instanceof ActionModelInterface && !$appleModel->actionAllowed($actionName))
        {
            $this->createFlash('Неразрешённое дейстие для яблока! Action: ' . $actionName, 'error');
            return false;
        }

        return $appleModel;
    }

    /**
     * Saves flash messag
     * @param string $message
     * @param string $type
     *
     * Available types: error, danger, success, info, warning
     */
    protected function createFlash($message, $type = 'success')
    {
        $session = Yii::$app->session;
        $session->addFlash($type, $message);
    }
}
