<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Apple model
 *
 * @property integer $id
 * @property string $color
 * @property integer $size
 * @property integer $status
 * @property integer $created_at
 * @property integer $falleddown_at
 */
class Apple extends ActiveRecord implements ActionModelInterface
{
    const STATUS_ON_TREE     = 10;
    const STATUS_FALLED_DOWN = 20;
    const STATUS_EATEN       = 30;
    const STATUS_ROTTEN      = 40;

    const TIME_ROT_SEC       = 5 * 60 * 60; // 5h

    protected $_colors = [
        'gold' => '#ffd700',
        'greenyellow' => '#adff2f',
        'limegreen' => '#32cd32',
        'red' => '#ff0000',
        'yellowgreen' => '#9acd32',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%apple}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => null,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['color', 'string', 'max' => 255],
            ['size', 'number', 'min' => 0, 'max' => 1],
            [['created_at', 'falleddown_at', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'color'         => 'Цвет',
            'size'          => 'Целостность',
            'status'        => 'Состояние',
            'created_at'    => 'Уродилось',
            'falleddown_at' => 'Упало',
        ];
    }

    /**
     * Apple constructor.
     *
     * @param array $color
     */
    public function __construct($color = null)
    {
        if(empty($color) || !isset($this->_colors[$color]))
        {
            $colorNames = array_keys($this->_colors);
            $colorIndex = rand(0, sizeof($colorNames) - 1);
            $color = $colorNames[$colorIndex];
        }
        $this->color = $color;
    }

    /**
     * Returns status of an apple
     * @return integer
     */
    public function calculateStatus()
    {
        if($this->falleddown_at == 0) return self::STATUS_ON_TREE;
        if($this->size == 0) return self::STATUS_EATEN;
        if($this->falleddown_at > 0 && $this->falleddown_at < time() - self::TIME_ROT_SEC) return self::STATUS_ROTTEN;
        return self::STATUS_FALLED_DOWN;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // Update apple status
        $this->status = $this->calculateStatus();

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        // Try to recalculate apple status - May be already rotten?
        $newStatus = $this->calculateStatus();
        if($newStatus != $this->status)
        {
            $this->status = $newStatus;
            $this->save();
        }

        parent::afterFind();
    }

    /**
     * Checks rights for action for selected model
     *
     * @param string $actionName
     * @return bool
     */
    public function actionAllowed($actionName)
    {
        $status = $this->calculateStatus();
        if(strcasecmp($actionName, 'falldown') == 0) return ($status == self::STATUS_ON_TREE);
        if(strcasecmp($actionName, 'eat') == 0) return ($status == self::STATUS_FALLED_DOWN);
        if(strcasecmp($actionName, 'delete') == 0) return ($status == self::STATUS_ROTTEN || $status == self::STATUS_EATEN);
        return true;
    }

    /**
     * Makes an apple fall down
     *
     * @return bool
     */
    public function fallToGround()
    {
        $this->falleddown_at = time();
        return $this->save();
    }

    /**
     * Eat an apple
     *
     * @param integer $percent
     * @return bool
     */
    public function eat($percent)
    {
        $applePart = intval($percent) / 100;
        $this->size -= $applePart;
        if($this->size < 0) $this->size = 0;
        return $this->save();
    }
}
