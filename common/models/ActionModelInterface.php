<?php

namespace common\models;

interface ActionModelInterface
{
    public function actionAllowed($actionName);
}
