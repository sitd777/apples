<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Apple search model
 *
 * @property integer $id
 * @property string $color
 * @property integer $size
 * @property integer $created_at
 * @property integer $falleddown_at
 */
class AppleSearch extends Apple
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Apple::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /*$query->andFilterWhere([
            'id' => $this->id,
            'iso_num' => $this->iso_num,
            'status' => $this->status,
            'created' => $this->created,
            'creator_id' => $this->creator_id,
            'modified' => $this->modified,
            'modifier_id' => $this->modifier_id,
        ]);

        $query->andFilterWhere(['like', 'iso_2', $this->iso_2])
            ->andFilterWhere(['like', 'iso_3', $this->iso_3])
            ->andFilterWhere(['like', 'name', $this->name]);*/

        return $dataProvider;
    }
}