<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Apple eat form
 */
class AppleEatForm extends Model
{
    /**
     * How much to eat
     * @var integer
     */
    public $eat = 0;

    /**
     * @var Apple
     */
    private $_apple;

    /**
     * AppleEatForm constructor.
     *
     * @param Apple $appleModel
     */
    public function __construct(Apple $appleModel)
    {
        $this->_apple = $appleModel;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['eat', 'required'],
            ['eat', 'integer', 'min' => 1, 'max' => $this->_apple->size * 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'eat' => 'Сколько процентов яблока съесть?',
        ];
    }
}
