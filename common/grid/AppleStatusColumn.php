<?php

namespace common\grid;

use common\models\Apple;
use yii\grid\DataColumn;

class AppleStatusColumn extends DataColumn
{
    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        switch($model->status)
        {
            case Apple::STATUS_ON_TREE:
                return 'На дереве';
            case Apple::STATUS_FALLED_DOWN:
                return 'На земле';
            case Apple::STATUS_EATEN:
                return 'Съедено';
            case Apple::STATUS_ROTTEN:
                return 'Гнилое';
        }
        return '&nbsp;';
    }
}