<?php

namespace common\grid;

use yii\grid\DataColumn;

class AppleColorColumn extends DataColumn
{
    /**
     * @inheritdoc
     */
    public function getDataCellValue($model, $key, $index)
    {
        $this->format = 'raw';

        // Register custom CSS
        \common\assets\AppleAsset::register(\Yii::$app->view);

        // return HTML for column
        return '<i class="glyphicon glyphicon-apple apple" aria-hidden="true" style="color: ' . $model->color . ';"></i> ' . $model->color;
    }
}