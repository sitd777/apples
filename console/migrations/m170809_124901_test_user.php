<?php

use yii\db\Migration;

class m170809_124901_test_user extends Migration
{
    public function up()
    {
        $this->insert('{{%user}}', [
            'username'             => 'test',
            'auth_key'             => \Yii::$app->security->generateRandomString(),
            'password_hash'        => \Yii::$app->getSecurity()->generatePasswordHash('test'),
            'password_reset_token' => \Yii::$app->security->generateRandomString(),
            'email'                => 'test@localhost',
            'status'               => \common\models\User::STATUS_ACTIVE,
            'created_at'           => time(),
            'updated_at'           => time(),
        ]);
    }

    public function down()
    {
        $this->delete('{{%user}}', '`username` = :user', ['user' => 'test']);
    }
}
