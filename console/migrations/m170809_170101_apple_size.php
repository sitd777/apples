<?php

use yii\db\Migration;

class m170809_170101_apple_size extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%apple}}', 'size', $this->decimal(3,2)->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->alterColumn('{{%apple}}', 'size', $this->smallInteger()->notNull()->defaultValue(100));
    }
}
