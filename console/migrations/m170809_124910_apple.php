<?php

use yii\db\Migration;

class m170809_124910_apple extends Migration
{
    public function up()
    {
        $this->createTable('{{%apple}}', [
            'id' => $this->primaryKey(),
            'color' => $this->string()->notNull()->defaultValue(''),
            'created_at' => $this->integer(),
            'falleddown_at' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'size' => $this->smallInteger()->notNull()->defaultValue(100),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%apple}}');
    }
}
